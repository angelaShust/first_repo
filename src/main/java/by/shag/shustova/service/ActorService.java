package by.shag.shustova.service;

import by.shag.shustova.api.dto.ActorDTO;
import by.shag.shustova.exception.EntityRepositoryException;
import by.shag.shustova.jpa.model.Actor;
import by.shag.shustova.jpa.repository.ActorRepository;
import by.shag.shustova.jpa.repository.ActorRepositoryImpl;
import by.shag.shustova.jpa.transaction.EntityTransaction;
import by.shag.shustova.mapping.ActorDTOMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ActorService {
    private final ActorRepository actorRepository = new ActorRepositoryImpl();
    private final ActorDTOMapper dtoMapper = new ActorDTOMapper();

    private List<Actor> list;
    private List<ActorDTO> actorDTOList;
    private EntityTransaction tx;
    private ActorDTO actorDTO;
    private Actor actor;

    public ActorDTO save(ActorDTO actorDTO) {
        try {
            tx = new EntityTransaction();
            tx.initTransaction(actorRepository);
            actor = new Actor();
            actor = dtoMapper.toConvertActor(actorDTO);
            actorRepository.save(actor);
            tx.commit();
        } catch (EntityRepositoryException e) {
            try {
                tx.rollback();
            } catch (EntityRepositoryException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                tx.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();
            }
        }
        return dtoMapper.toConvertActorDTO(actor);
    }

    public List<ActorDTO> foundCountry(String inCountry) {
        try {
            tx = new EntityTransaction();
            tx.withoutTransaction(actorRepository);
            list = new ArrayList<>();
            String country = inCountry.toUpperCase().trim();
            list = actorRepository.findByCountry(country);
            actorDTOList = new ArrayList<>();
            for (Actor element : list) {
                actorDTOList.add(dtoMapper.toConvertActorDTO(element));
            }
        } catch (EntityRepositoryException e) {
            e.printStackTrace();
        }
        return actorDTOList;
    }

    public ActorDTO foundID(Integer id) {
        try {
            tx = new EntityTransaction();
            tx.withoutTransaction(actorRepository);
            Optional<Actor> actor = actorRepository.findById(id);
            if (actor.isPresent()) {
                actorDTO = new ActorDTO();
                actorDTO = dtoMapper.toConvertActorDTO(actor.get());
                return actorDTO;
            }
        } catch (EntityRepositoryException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<ActorDTO> foundAll() {
        try {
            tx = new EntityTransaction();
            tx.withoutTransaction(actorRepository);
            list = new ArrayList<>();
            list = actorRepository.findAll();

            actorDTOList = new ArrayList<>();
            for (Actor element : list) {
                actorDTOList.add(dtoMapper.toConvertActorDTO(element));
            }
        } catch (EntityRepositoryException e) {
            e.printStackTrace();
        }
        return actorDTOList;
    }

    public ActorDTO put(ActorDTO actorDTO) {
        EntityTransaction tx = new EntityTransaction();
        try {
            tx = new EntityTransaction();
            tx.initTransaction(actorRepository);
            actor = new Actor();
            actor = dtoMapper.toConvertActor(actorDTO);
            actorRepository.update(actor);
            tx.commit();
        } catch (EntityRepositoryException e) {
            try {
                tx.rollback();
            } catch (EntityRepositoryException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                tx.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();
            }
        }
        return dtoMapper.toConvertActorDTO(actor);
    }

    public void delete(Integer id) {
        try {
            tx = new EntityTransaction();
            tx.initTransaction(actorRepository);
            actorRepository.deleteById(id);
            tx.commit();
        } catch (EntityRepositoryException e) {
            try {
                tx.rollback();
            } catch (EntityRepositoryException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                tx.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();
            }
        }
    }
}

