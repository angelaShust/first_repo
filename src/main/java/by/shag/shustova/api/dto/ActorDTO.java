package by.shag.shustova.api.dto;

import java.util.Objects;

public class ActorDTO {
    private Integer id;
    private String name;
    private String lastName;
    private String dateOfBirth;
    private String sex;
    private String country;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {

        this.country = country.toUpperCase().trim();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActorDTO actorDTO = (ActorDTO) o;
        return id.equals(actorDTO.id) && Objects.equals(name, actorDTO.name) &&
                Objects.equals(lastName, actorDTO.lastName) &&
                Objects.equals(dateOfBirth, actorDTO.dateOfBirth) &&
                Objects.equals(sex, actorDTO.sex) &&
                Objects.equals(country, actorDTO.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, lastName, dateOfBirth, sex, country);
    }

    @Override
    public String toString() {
        return "\nActorDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", \nsex='" + sex + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
