package by.shag.shustova.api.controller;

import by.shag.shustova.api.dto.ActorDTO;
import by.shag.shustova.service.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class ActorController {

    @Autowired
    private ActorService actorService;

    //ВСЕ АКТЕРЫ
    @GetMapping("/actor")
    public String findAll(Model model) {
        List<ActorDTO> all = actorService.foundAll();
        model.addAttribute("actors", all);
        return "actors.html";
    }

    //удаление
    @PostMapping("/deleteActor")
    public String deleteById(@ModelAttribute(name = "id") Integer id, Model model) {
        actorService.delete(id);
        return findAll(model);
    }

    //ПОИСК АКТЕРА по ID
    @GetMapping("/seekByID")
    public String seekID(Model model) {
        model.addAttribute("actor", new ActorDTO());
        return "findID.html";
    }

    @PostMapping("/seekByID")
    public String showActorByID(
            @RequestParam("id") Integer id,
            Model model) {
        ActorDTO actorDTO = actorService.foundID(id);
        model.addAttribute("actor", actorDTO);
        return "findIDActor.html";
    }

    //добавление актера
    @GetMapping(value = "/create")
    public String getCreateForm(Model model) {
        model.addAttribute("actor", new ActorDTO());
        return "actor-create.html";
    }

    @PostMapping("actor")
    public String createActor(
            @ModelAttribute("actor") ActorDTO actorDTO, Model model) {
        actorService.save(actorDTO);
        return findAll(model);
    }

    //редактирование актера
    @PostMapping("/forUpdateActor")
    public String put(@ModelAttribute(name = "id") Integer id, Model model) {
        ActorDTO actorDTO = actorService.foundID(id);
        model.addAttribute("actor", actorDTO);
        return "actor-update";
    }

    @PostMapping("/updateActor")
    public String put1(@ModelAttribute("actor") ActorDTO actorDTO, Model model) {
        actorService.put(actorDTO);
        return findAll(model);
    }

    //ПОИСК АКТЕРОВ по стране
    @GetMapping("/seekByCountry")
    public String findByCountry(Model model) {
        model.addAttribute("actor", new ActorDTO());
        return "findCountry.html";
    }

    @PostMapping("/seekByCountry")
    public String showActorByCountry(
            @ModelAttribute(name = "country") String country, Model model) {
        List<ActorDTO> actorDTOList = actorService.foundCountry(country);
        model.addAttribute("actor", actorDTOList);
        return "findIDActor.html";
    }
}
