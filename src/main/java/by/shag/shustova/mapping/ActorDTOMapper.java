package by.shag.shustova.mapping;

import by.shag.shustova.api.dto.ActorDTO;
import by.shag.shustova.jpa.model.Actor;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ActorDTOMapper {
    private Actor actor;
    private ActorDTO actorDTO;

    public ActorDTO toConvertActorDTO(Actor actor) {
        actorDTO = new ActorDTO();
        actorDTO.setId(actor.getId());
        actorDTO.setName(actor.getName());
        actorDTO.setLastName(actor.getLastName());
        actorDTO.setDateOfBirth(new java.sql.Date(actor.getDateOfBirth().getTime()).toString());
        actorDTO.setSex(actor.getSex());
        actorDTO.setCountry(actor.getCountry());
        return actorDTO;
    }

    public Actor toConvertActor(ActorDTO actorDTO) {
        actor = new Actor();
        actor.setId(actorDTO.getId());
        actor.setName(actorDTO.getName());
        actor.setLastName(actorDTO.getLastName());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            java.util.Date utilDate = format.parse(actorDTO.getDateOfBirth());
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            actor.setDateOfBirth(sqlDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        actor.setSex(actorDTO.getSex());
        actor.setCountry(actorDTO.getCountry());
        return actor;
    }


}
