package by.shag.shustova.mapping;

import by.shag.shustova.jpa.model.Actor;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ActorMapper implements EntityMapper<Actor> {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String LAST_NAME = "last_name";
    private static final String DATE_OF_BIRTH = "date_of_birth";
    private static final String SEX = "sex";
    private static final String COUNTRY = "country";

    @Override
    public Actor map(ResultSet rs) throws SQLException {
        Actor actor = new Actor();
        actor.setId(rs.getInt(ID));
        actor.setName(rs.getString(NAME));
        actor.setLastName(rs.getString(LAST_NAME));
        actor.setDateOfBirth(rs.getDate(DATE_OF_BIRTH));
        actor.setSex(rs.getString(SEX));
        actor.setCountry(rs.getString(COUNTRY));
        return actor;
    }

    @Override
    public void populate(Actor model, PreparedStatement statement) throws SQLException {
        statement.setString(1, model.getName());
        statement.setString(2, model.getLastName());
        statement.setDate(3, new Date(model.getDateOfBirth().getTime()));
        statement.setString(4, model.getSex());
        statement.setString(5, model.getCountry());
    }
}
