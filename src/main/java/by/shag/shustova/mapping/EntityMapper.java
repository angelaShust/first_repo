package by.shag.shustova.mapping;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface EntityMapper<E> {

    E map(ResultSet rs) throws SQLException;

    void populate(E model, PreparedStatement statement) throws SQLException;
}
