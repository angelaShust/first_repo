package by.shag.shustova.jpa.repository;

import by.shag.shustova.exception.EntityRepositoryException;
import by.shag.shustova.jpa.model.Actor;
import by.shag.shustova.mapping.ActorMapper;
import by.shag.shustova.mapping.EntityMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ActorRepositoryImpl implements ActorRepository {

    private final String FIND_BY_COUNTRY_QUERRY = "select * from actorss " +
            "where country = ?;";
    private final String SAVE_QUERRY = "insert into actorss values(default, ?, ?, ?, ?, ?);";
    private final String FIND_BY_ID_QUERRY = "select * from actorss as a " +
            "where a.id = ?;";
    private final String FIND_ALL_QUERRY = "select * from actorss;";
    private final String DELETE_BY_ID_QUERRY = "delete from actorss where id = ?;";
    private final String UPDATE_QUERRY = "update actorss set name = ?, " +
            "last_name = ?,  date_of_birth = ?, sex = ?, country = ? where id = ?;";

    private EntityMapper<Actor> actorMapper = new ActorMapper();
    private Connection connection;

    @Override
    public List<Actor> findByCountry(String country) throws EntityRepositoryException {
        try (PreparedStatement st = connection.prepareStatement(FIND_BY_COUNTRY_QUERRY)) {
            st.setString(1, country);
            ResultSet rs = st.executeQuery();
            List<Actor> result = new ArrayList<>();
            while (rs.next()) {
                result.add(actorMapper.map(rs));
            }
            return result;
        } catch (SQLException e) {
            throw new EntityRepositoryException("Ошибка Actor.findByCountry", e);
        }
    }

    @Override
    public Actor save(Actor entity) throws EntityRepositoryException {
        try (PreparedStatement st = connection.prepareStatement(SAVE_QUERRY)) {
            actorMapper.populate(entity, st);
            st.executeUpdate();
            return entity;
        } catch (SQLException e) {
            throw new EntityRepositoryException("Ошибка создания Actor.save", e);
        }
    }

    @Override
    public Optional<Actor> findById(Integer id) throws EntityRepositoryException {
        try (PreparedStatement st = connection.prepareStatement(FIND_BY_ID_QUERRY)) {
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            Optional<Actor> actorOptional = Optional.empty();
            if (rs.next()) {
                actorOptional = Optional.of(actorMapper.map(rs));
            }
            return actorOptional;
        } catch (SQLException e) {
            throw new EntityRepositoryException("Ошибка Actor.findById", e);
        }
    }

    @Override
    public List<Actor> findAll() throws EntityRepositoryException {
        try (PreparedStatement st = connection.prepareStatement(FIND_ALL_QUERRY)) {
            ResultSet rs = st.executeQuery();
            List<Actor> result = new ArrayList<>();
            while (rs.next()) {
                result.add(actorMapper.map(rs));
            }
            return result;
        } catch (SQLException e) {
            throw new EntityRepositoryException("Ошибка Actor.findAll", e);
        }
    }

    @Override
    public Actor update(Actor entity) throws EntityRepositoryException {
        try (PreparedStatement st = connection.prepareStatement(UPDATE_QUERRY)) {
            st.setInt(6, entity.getId());
            actorMapper.populate(entity, st);
            st.executeUpdate();
            return entity;
        } catch (SQLException e) {
            throw new EntityRepositoryException("Ошибка Actor.update", e);
        }
    }

    @Override
    public void deleteById(Integer id) throws EntityRepositoryException {
        try (PreparedStatement st = connection.prepareStatement(DELETE_BY_ID_QUERRY)) {
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            throw new EntityRepositoryException("Ошибка Actor.deleteById", e);
        }
    }

    @Override
    public void setConnection(Connection connection) throws EntityRepositoryException {
        this.connection = connection;
    }
}
