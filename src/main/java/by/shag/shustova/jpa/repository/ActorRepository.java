package by.shag.shustova.jpa.repository;

import by.shag.shustova.exception.EntityRepositoryException;
import by.shag.shustova.jpa.model.Actor;

import java.util.List;

public interface ActorRepository extends CRUDRepository<Actor, Integer> {

    List<Actor> findByCountry(String country) throws EntityRepositoryException;
}
