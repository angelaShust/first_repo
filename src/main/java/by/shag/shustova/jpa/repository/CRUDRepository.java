package by.shag.shustova.jpa.repository;

import by.shag.shustova.exception.EntityRepositoryException;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public interface CRUDRepository<E, ID> {
    //c
    E save(E entity) throws EntityRepositoryException;

    //r
    Optional<E> findById(ID id) throws EntityRepositoryException;

    //r
    List<E> findAll() throws EntityRepositoryException;

    //u
    E update(E entity) throws EntityRepositoryException;

    //d
    void deleteById(ID id) throws EntityRepositoryException;

    void setConnection(Connection connection) throws EntityRepositoryException;
}
