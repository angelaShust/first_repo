package by.shag.shustova.jpa.transaction;

import by.shag.shustova.exception.EntityRepositoryException;
import by.shag.shustova.jpa.repository.CRUDRepository;
import by.shag.shustova.jpa.repository.ConnectionCreator;

import java.sql.Connection;
import java.sql.SQLException;

public class EntityTransaction {
    private Connection connection;

    public void withoutTransaction(CRUDRepository... repositories) throws EntityRepositoryException {
        if (connection == null) {
            connection = ConnectionCreator.createConnection();
        }
        for (CRUDRepository repo : repositories) {
            repo.setConnection(connection);
        }
    }

    public void initTransaction(CRUDRepository... repositories) throws EntityRepositoryException {
        if (connection == null) {
            connection = ConnectionCreator.createConnection();
        }
        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new EntityRepositoryException(
                    "Ошибка отключения EntityTransaction.AutoCommit", e);
        }
        for (CRUDRepository repo : repositories) {
            repo.setConnection(connection);
        }
    }

    public void endTransaction() throws EntityRepositoryException {
        try {
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new EntityRepositoryException(
                    "Ошибка включения EntityTransaction.AutoCommit", e);
        }
    }

    public void commit() throws EntityRepositoryException {
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new EntityRepositoryException(
                    "Ошибка проведения транзакции EntityTransaction.commit", e);
        }
    }

    public void rollback() throws EntityRepositoryException {
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new EntityRepositoryException(
                    "Ошибка отмены транзакции EntityTransaction.rollback", e);
        }
    }
}
